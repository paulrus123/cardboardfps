## CardboardFPS

Author: Paul Sorenson

---

## Overview 

CardboardFPS is a tower-defense style game. The player needs to destroy as many enemies as possible before dying. 

Enemies will shoot projectiles at the player, which can be destroyed by looking at the projectile for enough time. 

The player can shoot at enemies with rockets by touching the screen. To reload the rockets, the player has to look down at the ammo box. 

---

## Hardware Requirements

Tested on Google Pixel 3. Not tested on iOS devices

Developed on MacOS

---

## How to deploy

To deploy the released APK: https://www.androidpit.com/android-for-beginners-what-is-an-apk-file
To Deploy from Unity Editor: 
   Make sure the build settings are set correcly for Android and Google Cardboard. Then use File-->Build and Run to deploy to a connected device

---

## Dependencies
Google VR SDK: https://developers.google.com/vr/develop/unity/download
Sound effects obtained from https://www.zapsplat.com
Laser: Contributor : Alexander Gastrell - https://www.zapsplat.com/music/science-fiction-weapon-laser-hit-1/
Shotgun: Contributor : John J - https://www.zapsplat.com/music/shotgun-reload-slight-distance-from-microphone-some-room-reverb/
Sci-fi Thud - https://www.zapsplat.com/music/sci-fi-thud-generic-impact-1/

## References
Bezier Curve Calculation: https://answers.unity.com/questions/650526/how-can-i-make-an-missilerocket-home-to-an-enemy-i.html (Hamdullahshah answer)

