﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class LaserReticleBehavior : MonoBehaviour
{
    AntiProjectileLaser _laser;

    private void Start()
    {
        _laser = GameObject.Find("LaserRenderer").GetComponent<AntiProjectileLaser>();
    }

    public void SetFillAmount(float val)
    {
        GetComponent<Image>().fillAmount = val;
    }

    public void FireLaser()
    {
        _laser.FireLaser();
    }
}
