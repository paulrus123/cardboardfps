﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject reticleFill;
    float interval = 1.5f;
    float timer;

    bool isFocused = false;

    //Detect if the Cursor starts to pass over the GameObject
    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        isFocused = true;
    }

    //Detect when Cursor leaves the GameObject
    public void OnPointerExit(PointerEventData pointerEventData)
    {
        isFocused = false;
    }

    private void Update()
    {
        if (isFocused)
        {
            timer += Time.deltaTime;

            if (timer > interval)
            {
                timer = 0;
                LoadLevel1();
            }
        }
        else
        {
            timer = 0;
        }

        SetFillAmount(timer / interval);
    }

    public void SetFillAmount(float val)
    {
        reticleFill.GetComponent<Image>().fillAmount = val;
    }

    void LoadLevel1()
    {
        SceneManager.LoadScene("Level_1");
    }

}
