﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class AmmoHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    bool isFocused = false; //if user is looking at the ammo box
    float reloadInterval = 0.25f;
    float timer;
    public AudioSource reloadAudio;


    public GameObject rocket_1, rocket_2, rocket_3, rocket_4, rocket_5; //rocket icons

    public int numRockets = 5; //number of rockets user has

    //Detect if the Cursor starts to pass over the GameObject
    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        isFocused = true;
    }

    //Detect when Cursor leaves the GameObject
    public void OnPointerExit(PointerEventData pointerEventData)
    {
        isFocused = false;
    }

    private void Update()
    {
        if(isFocused)
        {
            timer += Time.deltaTime;

            if(timer > reloadInterval)
            {
                timer = 0;
                IncrementRockets(1); //add a rocket
            }
        }
        else
        {
            timer = 0;
        }
    }

    public void IncrementRockets(int amount)
    {
        if(((numRockets + amount) >= 0 ) && ((numRockets + amount) <= 5))
        {
            numRockets += amount;
            UpdateIcons();

            if (amount > 0)
                reloadAudio.Play();
        }
    }

    void UpdateIcons()
    {
        rocket_1.SetActive((numRockets >= 1 ? true : false));
        rocket_2.SetActive((numRockets >= 2 ? true : false));
        rocket_3.SetActive((numRockets >= 3 ? true : false));
        rocket_4.SetActive((numRockets >= 4 ? true : false));
        rocket_5.SetActive((numRockets >= 5 ? true : false));
    }
}
