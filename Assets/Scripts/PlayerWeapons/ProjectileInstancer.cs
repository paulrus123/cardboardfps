﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileInstancer : MonoBehaviour
{
    public GameObject ProjectilePrefab;
    public float maxPressTime; //max time you can press the screen to fire, if > maxPressTime then will start reloading
    public AmmoHandler ammoHandler;

    public AudioSource _launchAudio;

    //is input pressed (down, up, isPressed)
    bool keyDown;
    bool keyUp;
    bool keyIsPressed;

    //time when key was pressed down
    private float timeOfKeyDown;

    void Update()
    {
        //Set keyUp, keyDown, keyIsPressed based on user Input
        handleInput();

        if (keyDown)
        {
            timeOfKeyDown = Time.time;
        }
        if (keyUp)
        {
            if ((Time.time - timeOfKeyDown) <= maxPressTime)
            {
                InstanceProjectile();
            }
            else
            {
                Debug.Log("Too long - don't fire rocket");
            }
        }
    }

    //Instances a missile and fires it in the direction of the camera
    void InstanceProjectile()
    {
        if (ammoHandler.numRockets > 0)
        {
            ammoHandler.IncrementRockets(-1);
            Quaternion projectileRotation = transform.rotation;
            var projectile = Instantiate(ProjectilePrefab, transform.position, transform.rotation);
            projectile.GetComponent<ProjectileBehavior>().Fire();
            _launchAudio.Play();
        }
    }

    //handles input from touchscreen (running on mobile device) or spaceBar if running in editor
    void handleInput()
    {

#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.Space)) { keyIsPressed = true; }
        else { keyIsPressed = false; }

        if (Input.GetKeyDown(KeyCode.Space)) { keyDown = true; }
        else { keyDown = false; }

        if (Input.GetKeyUp(KeyCode.Space)) { keyUp = true; }
        else { keyUp = false; }
#else
        if (Input.touchCount > 0) { keyIsPressed = true; }
        else { keyIsPressed = false; }

        if (Input.GetTouch(0).phase == TouchPhase.Began) { keyDown = true; }
        else { keyDown = false; }

        if (Input.GetTouch(0).phase == TouchPhase.Ended) { keyUp = true; }
        else { keyUp = false; }
#endif
}
}
