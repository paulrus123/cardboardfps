﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehavior : MonoBehaviour
{
    int projectileVelocity = 250;
    int maxLifeTime = 10; 

    // Start is called before the first frame update
    void Start()
    {
        //Destroy if haven't hit anything in 10 seconds
        Destroy(gameObject, maxLifeTime);
    }

    // Update is called once per frame
    public void Fire()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * projectileVelocity); 
    }
}
