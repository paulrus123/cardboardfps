﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//A Laser to destroy incoming projectiles
public class AntiProjectileLaser : MonoBehaviour
{
    LineRenderer line;
    public Camera MainCamera;

    private void Start()
    {
        line = gameObject.GetComponent<LineRenderer>();
        line.enabled = false;
    }


    public void FireLaser()
    {
        StartCoroutine("FireEnumerator");
    }

    IEnumerator FireEnumerator()
    {
        line.enabled = true;
        float timeOfLaser = 0f;

        while(timeOfLaser < 0.25f)
        {
            timeOfLaser += Time.deltaTime;
            Ray ray = new Ray(MainCamera.transform.position, transform.forward);

            line.SetPosition(0, transform.position);
            line.SetPosition(1, ray.GetPoint(100));

            yield return null;
        }

        line.enabled = false;
    }
}
