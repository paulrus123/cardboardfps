﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundHandler : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if ((collision.gameObject.tag == "EnemyProjectile") || (collision.gameObject.tag == "Missile"))
        {
            Destroy(collision.gameObject);
            //TODO: explosion
        }
    }
}
