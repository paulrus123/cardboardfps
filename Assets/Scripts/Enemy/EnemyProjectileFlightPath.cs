﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileFlightPath : MonoBehaviour
{
    public float duration = 6f;
    public float controlHeight = 30f;

    Vector3 origin;
    Vector3 startPosition;
    Vector3 controlPoint;
    float currentDuration;

    // Start is called before the first frame update
    void Start()
    {
        currentDuration = 0;
        startPosition = transform.position;
        origin = new Vector3(0, 0, 0);
        controlPoint = new Vector3(startPosition.x * 0.5f, controlHeight, startPosition.z * 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = CalculateBezierPoint(currentDuration / duration, startPosition, origin, controlPoint);
        currentDuration += Time.deltaTime;
    }

    private Vector3 CalculateBezierPoint(float t, Vector3 _startPosition, Vector3 _endPosition, Vector3 _controlPoint)
    {
        float u = 1 - t;
        float uu = u * u;

        Vector3 point = uu * _startPosition;
        point += 2 * u * t * _controlPoint;
        point += t * t * _endPosition;

        return point;
    }

}
