﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{
    float shootTime = 6.0f;
    float timer = 0.0f;
    public GameObject enemyProjectile;
     

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer > shootTime)
        {
            LaunchProjectile();
            timer = Random.Range(0.0f, 2.0f);
        }
    }

    void LaunchProjectile()
    {
        Instantiate(enemyProjectile,new Vector3(transform.position.x, transform.position.y + 3f, transform.position.z),Quaternion.identity);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Missile")
        {
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }
}
