﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class EnemyProjectileHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    //Flight Path data
    public float duration = 6f;
    public float controlHeight = 30f;
    Vector3 origin;
    Vector3 startPosition;
    Vector3 controlPoint;
    float currentDuration;

    //Laser Data
    float timeToLaser = 0.75f;
    float timeCounter;
    bool isFocused;
    LaserReticleBehavior laser;
    GameObject player;
    AudioSource laserAudio;

    private void Start()
    {
        player = GameObject.Find("PlayerCollider");
        laser = GameObject.Find("LaserFill").GetComponent<LaserReticleBehavior>();
        laserAudio = GameObject.Find("LaserAudio").GetComponent<AudioSource>();

        //Flight Path
        currentDuration = 0;
        startPosition = transform.position;
        origin = new Vector3(0, 0, 0);
        controlPoint = new Vector3(startPosition.x * 0.5f, controlHeight, startPosition.z * 0.5f);
    }

    //Detect if the Cursor starts to pass over the GameObject
    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        //Output to console the GameObject's name and the following message
        Debug.Log("Cursor Entering " + name + " GameObject");
        isFocused = true;
    }

    //Detect when Cursor leaves the GameObject
    public void OnPointerExit(PointerEventData pointerEventData)
    {
        //Output the following message with the GameObject's name
        Debug.Log("Cursor Exiting " + name + " GameObject");
        timeCounter = 0f;
        isFocused = false;
        laser.SetFillAmount(0);
    }

    private void Update()
    {
        if(isFocused)
        {
            timeCounter += Time.deltaTime;
            laser.SetFillAmount(timeCounter / timeToLaser);

            if (timeCounter > timeToLaser)
            {
                //Destroy
                laser.FireLaser();
                laser.SetFillAmount(0);
                timeCounter = 0;
                Destroy(gameObject);
                laserAudio.Play();
            }
        }

        //Flight Path
        this.transform.position = CalculateBezierPoint(currentDuration / duration, startPosition, origin, controlPoint);
        currentDuration += Time.deltaTime;
    }

    private Vector3 CalculateBezierPoint(float t, Vector3 _startPosition, Vector3 _endPosition, Vector3 _controlPoint)
    {
        float u = 1 - t;
        float uu = u * u;

        Vector3 point = uu * _startPosition;
        point += 2 * u * t * _controlPoint;
        point += t * t * _endPosition;

        return point;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerHealth>().UpdateHealth(-10); 
            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "Ground")
        {
            Destroy(gameObject);
        }
    }
}