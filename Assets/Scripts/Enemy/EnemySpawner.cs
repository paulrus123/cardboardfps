﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;
    public float enemySpawnTime;

    float timer = 4.0f;


    private void Update()
    {
        timer += Time.deltaTime;
        if(timer > enemySpawnTime)
        {
            timer = 0f;
            SpawnRobot();
        }
    }

    void SpawnRobot()
    {
        //get random angle and distance
        float randAngle = UnityEngine.Random.Range(0.0f, (float)(2 * Math.PI));
        float randDistance = UnityEngine.Random.Range(10f, 40f); //do not spawn enemy closer than 10 meters

        //convert polar to cartesian
        float y = 10f; //10 meters up
        float x = (float) (randDistance * Math.Cos(randAngle));
        float z = (float) (randDistance * Math.Sin(randAngle));

        Instantiate(enemyPrefab, new Vector3(x, y, z), Quaternion.identity);
    }
}
