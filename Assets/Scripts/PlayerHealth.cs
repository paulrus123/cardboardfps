﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public GameObject healthFill;
    int health = 100;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "EnemyProjectile")
        {
            UpdateHealth(-10);
            Destroy(collision.gameObject);
        }
    }

    public void UpdateHealth(int amount)
    {
        health += amount;
        healthFill.GetComponent<Image>().fillAmount = health * .01f; 

        if(health <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
